# -*- coding: utf-8 -*-
from . import blockmesher



def create_box(xmin, xmax, ymin, ymax, zmin, zmax, opath, nx=10, ny=10, nz=10):
    
    m = blockmesher.create_single_block_mesh(xmin, xmax, ymin, ymax, zmin, zmax, nx, ny, nz)
    w = blockmesher.Writer()
    w.write(m, opath)




def create_toposet_box(xmin, xmax, ymin, ymax, zmin, zmax, opath):

    from .point import Point
    from .toposetdict import TopoSetDict
    from .toposet_action_box import ToposetActionBox

    d = TopoSetDict()
    p1 = Point(xmin, ymin, zmin)
    p2 = Point(xmax, ymax, zmax)

    
    action = ToposetActionBox(p1, p2, "TOPOBOX")
    
    d.add_action(action)
    d.write(opath)

    
