# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

setup(
    name='blockmesher',
    version='0.1.0',
    description='Tool for generating blockMeshDicts',
    long_description=readme,
    author='Petteri Peltonen',
    author_email='petteri.peltonen@aalto.filter',
    #url='https://peltonp1@bitbucket.org/peltonp1/blockmesher.git',
    #url='https://bitbucket.org/peltonp1/blockmesher',
    packages=find_packages(exclude=('tests', 'docs'))
)

