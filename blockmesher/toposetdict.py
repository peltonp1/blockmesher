# -*- coding: utf-8 -*-

from .of_header import OfHeader

class TopoSetDict(object):

    def __init__(self):
        self.header = OfHeader("topoSetDict")
        self.actions = []


    def write(self, path):

        self.f = open(path,"w+")

        self.__write_header()
        self.__write_actions()
        

        self.f.close()
    
    def __write_header(self):
        
        string = self.header.get_string()   
        self.f.write(string)

    def __write_actions(self):

        self.f.write("actions\n(\n")    

        for action in self.actions:
            string = action.get_string()
            self.f.write(string + 5*'\n')
        self.f.write(");\n")

    def add_action(self, action):

        self.actions.append(action)

    
