# -*- coding: utf-8 -*-

class OfHeader(object):
    

    def __init__(self, obj_type):

        self.obj_type = obj_type
        self.break_str = "/****************************************************************/\n"
    def get_string(self):
                
        string = self.break_str
        string +="\
            FoamFile\n\
            {{\n\
            version     2.0;\n\
            format      ascii;\n\
            class       dictionary;\n\
            object      {0};\n\
            }}\n".format(self.obj_type)

        string +=self.break_str
        return string
        
