# -*- coding: utf-8 -*-



class ToposetActionBox(object):
    
    """
    {
        name    c0;
        type    cellSet;
        action  new;
        source  boxToCell;
        sourceInfo
        {
                 box (-0.45 -0.5 1.3) (0.8 0.45 2.2); //For Re10m outlet bl thickness is 0.16
        }
    }
    """
    
    def __init__(self, p_min, p_max, name, type="cellSet", action="new", source="boxToCell"):
        
        self.p_min = p_min
        self.p_max = p_max
        self.name = name
        self.type = type
        self.action = action
        self.source = source
    
    def __sourceInfo(self):

        string = "sourceInfo\n\
                 {{\n\
                 \tbox ({0} {1} {2}) ({3} {4} {5});\n\
                 }}\n".format(self.p_min.x, self.p_min.y, self.p_min.z, self.p_max.x, self.p_max.y, self.p_max.z)
 
        return string


    def get_string(self):

        string ="\
            {{\n\
            name {0};\n\
            type {1};\n\
            action {2};\n\
            source {3};\n\
            {4}\n\
            }}\n".format(self.name, self.type, self.action, self.source, self.__sourceInfo())
        
        
        return string
