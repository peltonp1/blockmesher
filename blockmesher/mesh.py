# -*- coding: utf-8 -*-

class Mesh(object):

    def __init__(self, points=[], blocks=[], edges=[], patches=[]):

        self.points = points
        self.blocks = blocks
        self.edges = edges
        self.patches = patches
