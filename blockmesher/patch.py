# -*- coding: utf-8 -*-

class Patch(object):

    def __init__(self, name, type, faces):

        self.name = name
        self.type = type
        self.faces = faces
        

    def get_string(self):

        string = self.name + "\n {"
        string+= "  type {};\n".format(self.type)
        string+= "  faces \n"
        string+= "  (\n"
    
        for face in self.faces:
            string+="({} {} {} {})".format(face[0], face[1], face[2], face[3]) + "\n"
    
        string+="   );\n"
        string+=" }\n"
        return string
