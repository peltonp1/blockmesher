import numpy as np

from .point import Point
from .block   import Block
from .patch import Patch
from .writer import Writer
from .mesh import Mesh


def create_single_block_mesh(xmin, xmax, ymin, ymax, zmin, zmax, nx, ny, nz):

    x_coords = [xmin, xmax]
    y_coords = [ymin, ymax]
    z_coords = [zmin, zmax]
    
    
    points = create_points(x_coords, y_coords, z_coords)
    blocks = create_blocks(points)
    blocks[0,0,0].nx = nx; blocks[0,0,0].ny = ny; blocks[0,0,0].nz = nz;

    patches = []

    p = Patch("x0", "patch", [ blocks[0,0,0].neg_xnormal_face() ])
    patches.append(p)
    
    p = Patch("x1", "patch", [ blocks[0,0,0].pos_xnormal_face() ])
    patches.append(p)
    
    p = Patch("y0", "patch", [ blocks[0,0,0].neg_ynormal_face() ])
    patches.append(p)

    p = Patch("y1", "patch", [ blocks[0,0,0].pos_ynormal_face() ])
    patches.append(p)    

    p = Patch("z0", "patch", [ blocks[0,0,0].neg_znormal_face() ])
    patches.append(p)  

    p = Patch("z1", "patch", [ blocks[0,0,0].pos_znormal_face() ])
    patches.append(p)  

    edges = []    
        
    mesh = Mesh(points, blocks, edges, patches)
    return mesh

def create_points(x_coords, y_coords, z_coords):
    
    nx = len(x_coords)
    ny = len(y_coords)
    nz = len(z_coords)
   
    points = np.empty((nx, ny, nz), dtype=object)
    
    idx = 0
    
    for i in range(nx):
        for j in range(ny):
            for k in range(nz):
            
                p = Point(x_coords[i], y_coords[j], z_coords[k], idx)
                points[i,j,k] = p
                idx+=1
                
    return points
    

def create_blocks(points):

 
    points_imax = points.shape[0]
    points_jmax = points.shape[1]
    points_kmax = points.shape[2]
    
    blocks = np.empty((points_imax - 1, points_jmax - 1, points_kmax - 1), dtype=object)
    
    idx = 0    
    for i in range(points_imax - 1):
        for j in range(points_jmax - 1):
            for k in range(points_kmax - 1):
                
                #lower
                p0 = points[i,j,k]
                p1 = points[i+1, j, k]
                p2 = points[i+1, j+1, k]
                p3 = points[i, j+1, k]
                
                #upper
                p4 = points[i,j,k+1]
                p5 = points[i+1, j, k+1]
                p6 = points[i+1, j+1, k+1]
                p7 = points[i, j+1, k+1]


                blocks[i,j,k] = Block(p0, p1, p2, p3, p4, p5, p6, p7, idx, nx=10, ny = 10, nz = 10)
                
                
                idx+=1
    
    return blocks



    
def create_patches(blocks,):
    
    
    patches = []    
    
    blocks_imax = blocks.shape[0]
    blocks_jmax = blocks.shape[1]
    blocks_kmax = blocks.shape[2]
    
    """
    #side wall patches
    y0_faces = []
    y1_faces = []
    
    for i in range(blocks_imax):
        for k in range(blocks_kmax):
        
            y0_faces.append(blocks[i,0,k].neg_ynormal_face())
            y1_faces.append(blocks[i,blocks_jmax-1,k].pos_ynormal_face())    
    
    
    Side1 = Patch("Side1", "patch", y0_faces)
    Side2 = Patch("Side2", "patch", y1_faces)
    
    #top and bottom patches
    top_faces = []
    bottom_faces = []
    
    for i in range(blocks_imax):
        for j in range(blocks_jmax):
        
            top_faces.append(blocks[i,j,blocks_kmax-1].pos_znormal_face())
            bottom_faces.append(blocks[i,j,0].neg_znormal_face())    
    
    
    Top = Patch("Top", "patch", top_faces)
    Bottom = Patch("Bottom", "patch", bottom_faces)
    
    patches.append(Top)
    patches.append(Bottom)
    
    #j = 11 corresponds to the centermost block in y-direction
    
    i = 0
    j = 11
    k = 1
    Nozzle1 = Patch("Nozzle1", "patch", [ blocks[i,j,k].neg_xnormal_face() ])
    patches.append(Nozzle1)
    
    
    i = 0
    j = 11 + 6
    k = 1
    Nozzle2 = Patch("Nozzle2", "patch", [ blocks[i,j,k].neg_xnormal_face() ])
    patches.append(Nozzle2)
    
    
    i = 0
    j = 11 - 6
    k = 1
    Nozzle3 = Patch("Nozzle3", "patch", [ blocks[i,j,k].neg_xnormal_face() ])
    patches.append(Nozzle3)
    
    
    
    
    #on the other side you have to call pos_xnormal_face()
    ###########
    i = 0
    j = 11 + 4
    k = 1
    Nozzle4 = Patch("Nozzle4", "patch", [ blocks[i,j,k].pos_xnormal_face() ])
    patches.append(Nozzle4)
    
    i = 0
    j = 11 - 4
    k = 1
    Nozzle5 = Patch("Nozzle5", "patch", [ blocks[i,j,k].pos_xnormal_face() ])
    patches.append(Nozzle5)
    """
        
    return patches    
