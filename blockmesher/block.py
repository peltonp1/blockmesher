# -*- coding: utf-8 -*-

class Block(object):
    
    
    
    def __init__(self, p1, p2, p3 , p4, p5, p6, p7, p8, num, nx=10, ny=10, nz=10):
        
        self.p1 = p1; self.p2 = p2; self.p3 = p3; self.p4 = p4;
        self.p5 = p5; self.p6 = p6; self.p7 = p7; self.p8 = p8;
        
        self.nx = nx; self.ny = ny; self.nz = nz;
          
        self.n = num
        
        
    ##These are handy for creating patches from blocks    
    def pos_ynormal_face(self):
        return [self.p4.n, self.p3.n, self.p7.n, self.p8.n]

    def neg_ynormal_face(self):
        return [self.p1.n, self.p2.n, self.p6.n, self.p5.n]
    
    def neg_xnormal_face(self):
        return [self.p1.n, self.p4.n, self.p8.n, self.p5.n]

    def pos_xnormal_face(self):
        return [self.p3.n, self.p2.n, self.p6.n, self.p7.n]

    def pos_znormal_face(self):
        return [self.p6.n, self.p7.n, self.p8.n, self.p5.n]

    def neg_znormal_face(self):
        return [self.p1.n, self.p4.n, self.p3.n, self.p2.n]    

        
    def get_string(self):

        string = "hex ({} {} {} {} {} {} {} {})".format(self.p1.n, \
                                         self.p2.n, \
                                         self.p3.n, \
                                         self.p4.n, \
                                         self.p5.n, \
                                         self.p6.n, \
                                         self.p7.n, \
                                         self.p8.n)
        
        string+=" ({} {} {})".format(self.nx, self.ny, self.nz)
        
        string+=" simpleGrading (1 1 1) "
        
        
        return string
