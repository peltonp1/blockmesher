# -*- coding: utf-8 -*-


from .of_header import OfHeader

class Writer(object):
    
    
        

    def write(self, mesh, path):

        self.f = open(path,"w+")

        self.write_header()
        self.write_points(mesh.points)
        self.write_blocks(mesh.blocks)
        self.write_edges(mesh.edges)
        self.write_patches(mesh.patches)

        self.f.close()

    def write_header(self):
        
        header = OfHeader("blockMeshDict")
        string = header.get_string()        

        #self.f.write(string + 10*'\n')

        #string = "convertToMeters 1;"
        self.f.write(string)


    def write_points(self, points):
        
        self.f.write(5*"\n")
        self.f.write("vertices\n")
        self.f.write("(\n")

        points_imax = points.shape[0]
        points_jmax = points.shape[1]
        points_kmax = points.shape[2]
        
        for i in range(points_imax):
            for j in range(points_jmax):
                for k in range(points_kmax):
                
                    string = "( {} {} {} )  //{}\n".format(points[i,j,k].x, points[i,j,k].y, points[i,j,k].z, points[i,j,k].n)
                    self.f.write(string)
        self.f.write(");")
        
        
    
    def write_blocks(self, blocks):

        

        self.f.write(5*"\n")
        self.f.write("blocks\n")
        self.f.write("(\n")
    
        
    
        for block in blocks.flatten():
            string = block.get_string()
            self.f.write(string + "\n")
        
        self.f.write("\n);")            

    def write_edges(self, edges):
        self.f.write(5*"\n")
        self.f.write("edges\n")
        self.f.write("(\n")
        for edge in edges:
            self.f.write(edge.get_string() + "\n")
        self.f.write(");")

    def write_patches(self, patches):
        self.f.write(5*"\n")
        self.f.write("boundary\n")
        self.f.write("(\n")
    
        for patch in patches:
            self.f.write(patch.get_string())
            self.f.write(5*"\n")
    
    
        self.f.write(");\n")            
    
        self.f.write("mergePatchPairs ();")
